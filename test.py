#!/usr/bin/python3
# encoding: utf-8

import traceback
import logging
import queue

from states import StartState, SentinelState
from stateful import MultiuserStateMachine
from backends import UnitTestBackend


logging.basicConfig(format='%(asctime)s    [%(levelname)s]    %(message)s',
                    datefmt='%d.%m.%Y %H:%M:%S',
                    level=logging.DEBUG)

dialogue = '''
12223822 > /start

12223822 < Привет, <b>12223822</b>!
Это телеграм-бот, при помощи которого можно задавать вопросы и голосовать за них во время дебатов по истории и обществознанию

12223822 < Выберите класс

12223822 > 10б

12223822 < Выбран класс 10Б.

12223822 < Отправьте вопрос команде правительства.

12223822 > Вопрос ##

1463706336 < Отправлен вопрос команде правительства: <b>Вопрос ##</b>
Автор: <b>12223822</b>

12223822 < Вопрос "<b>Вопрос ##</b>" переслан.

12223822 < Отправьте вопрос команде правительства.

12223822 > первому столу

12223822 < Отправьте вопрос первому столу правительства.

12223822 > премьер-министру

12223822 < Отправьте вопрос премьер-министру.

12223822 > команде оппозиции

12223822 < Отправьте вопрос лидеру оппозиции.

12223822 > Вопрос ###

1463706336 < Отправлен вопрос лидеру оппозиции: <b>Вопрос ###</b>
Автор: <b>12223822</b>

12223822 < Вопрос "<b>Вопрос ###</b>" переслан.

12223822 < Отправьте вопрос лидеру оппозиции.
'''

# intentionally not wrapped in `if __name__ == '__main__':`
# tests should be executed when file is imported

try:
    machine = MultiuserStateMachine(StartState)
    backend = UnitTestBackend(dialogue)
    
    messages = queue.SimpleQueue()
    
    while not backend.test_succeeded():
        if machine.state_is(SentinelState):
            raise Exception('[TEST]    Machine tried to stop before test end')
        
        if messages.empty() and machine.needs_message():
            for e in backend.receive_all_new_messages():
                logging.info(f'[TEST]    Putting message into queue: {e}')
                messages.put_nowait(e)
        
        if not machine.needs_message():
            machine.next(backend, None)
        elif not messages.empty():
            msg = messages.get_nowait()
            
            logging.info(f'[TEST]    Processing message from queue: {msg}')
            machine.next(backend, msg)
    
    logging.info('[TEST]    success')
except:
    logging.error(f'[TEST]    failed: {traceback.format_exc()}')
finally:
    if __name__ == '__main__':
        input('...')
