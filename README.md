# Project 'Debate society'

## About bot

This bot allows students in class to send questions to teams during debates.

## Tech stack

As this bot is multi-backend, `pyTelegramBotAPI` was rejected as library limiting bot architecture too much.
So there is support for Telegram, [Stem](https://stem.fomalhaut.me), integration with other messengers can also be easily added.

Moreover, this bot supports unit-testing! Launch `test.py` and you'll see it. Tests allow us to know that bot is actually working before deploying it.

## Team RICA (Raccoon Is Competing Again)
- Andrey - **team lead**, **senior** Python, JS, FunC, Lua, **middle** Rust, C++ **developer**
  - designed bot architecture, created convenient wrappers
  - converted `pyTelegramBotAPI` code into one compatible with own wrappers
  - created presentation as well as its introduction video
  - presented the project
- Maksim - **junior** Python **developer**
  - written down part of bot code (take a look on 99abe780)
  - had fun and good luck

## TODOs
- [ ] Spreading across schools (if there are debates anywhere else)
- [ ] Make the bot compatible with lectures, hackatons, etc
