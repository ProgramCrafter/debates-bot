import functools
import logging
import html
import json
import time
import os

from stateful import IState, MultiuserStateMachine, RegisterState


def flatten(arr):
    for row in arr:
        yield from row


@RegisterState
class StartState(IState):
    '''
    Welcome state, where everyone not yet known to bot starts.
    '''
    def needs_message(self):
        return True
    
    def enter_state(self, message_info, reply, send_callback):
        pass
    
    def run(self, message_info, reply, send_callback):
        chat_id, message, message_id = message_info
        
        reply(f'''
Привет, <b>{chat_id}</b>!
Это телеграм-бот, при помощи которого можно задавать вопросы и голосовать за них во время дебатов по истории и обществознанию
'''.strip())
        
        next_state = ClassSelectState()
        next_state.enter_state(message_info, reply, send_callback)
        return next_state
    
    def __repr__(self):
        return ''
    
    @staticmethod
    def load(state_repr):
        return StartState()


@RegisterState
class ClassSelectState(IState):
    '''
    Requesting student for his grade and class letter.
    '''
    def needs_message(self):
        return True
    
    def enter_state(self, message_info, reply, send_callback):
        reply('Выберите класс', keyboard=[
          ['10а', '10б', '10в'],
          ['11а', '11б', '11в']
        ])
    
    def run(self, message_info, reply, send_callback):
        chat_id, message, message_id = message_info
        
        if message not in ('10а', '10б', '10в', '11а', '11б', '11в'):
            self.enter_state(message_info, reply, send_callback)
            return self
        
        reply(f'Выбран класс {message.upper()}.')
        
        next_state = AskingState()
        next_state.user_class = message
        next_state.enter_state(message_info, reply, send_callback)
        return next_state
    
    def __repr__(self):
        return ''
    
    @staticmethod
    def load(state_repr):
        return ClassSelectState()


@RegisterState
class AskingState(IState):
    '''
    The state where debates participant is asked to whom send the question.
    '''
    def __init__(self):
        # Question target formatting
        # 1. Q/C - ask Question or leave a Comment
        # 2. L/R - supporting or another side
        # 3. 1/2 - desk number
        # 4. 1/2 - participant number
        #        S     - end at current step
        
        self.question_target = 'QLS'
        
    def needs_message(self):
        return True
    
    def enter_state(self, message_info, reply, send_callback):
        reply(f'Отправьте {self.format_target()}.',
              keyboard=self.format_keyboard(self.question_target))
    
    # TODO: maybe factor out to a separate class
    def format_target(self):
        'Decrypting the code and returning the desired message'
        
        if self.question_target == 'QS':         return 'вопрос всем'
        elif self.question_target == 'CS':       return 'комментарий к дебатам в общем'
        
        elif self.question_target == 'QLS':      return 'вопрос команде правительства'
        elif self.question_target == 'QRS':      return 'вопрос команде оппозиции'
        elif self.question_target == 'CLS':      return 'комментарий о команде правительства'
        elif self.question_target == 'CRS':      return 'комментарий о команде оппозиции'
        
        elif self.question_target == 'QL1S':     return 'вопрос первому столу правительства'
        elif self.question_target == 'QL2S':     return 'вопрос второму столу правительства'
        elif self.question_target == 'QR1S':     return 'вопрос первому столу оппозиции'
        elif self.question_target == 'QR2S':     return 'вопрос второму столу оппозиции'
        elif self.question_target == 'CL1S':     return 'комментарий о выступлении первого стола правительства'
        elif self.question_target == 'CL2S':     return 'комментарий о выступлении второго стола правительства'
        elif self.question_target == 'CR1S':     return 'комментарий о выступлении первого стола оппозиции'
        elif self.question_target == 'CR2S':     return 'комментарий о выступлении второго стола оппозиции'
        
        elif self.question_target == 'QL11S':    return 'вопрос премьер-министру'
        elif self.question_target == 'QL12S':    return 'вопрос заместителю премьер-министра'
        elif self.question_target == 'QL21S':    return 'вопрос члену правительства'
        elif self.question_target == 'QL22S':    return 'вопрос секретарю правительства'
        elif self.question_target == 'QR11S':    return 'вопрос лидеру оппозиции'
        elif self.question_target == 'QR12S':    return 'вопрос заместителю лидера оппозиции'
        elif self.question_target == 'QR21S':    return 'вопрос члену оппозиции'
        elif self.question_target == 'QR22S':    return 'вопрос секретарю оппозиции'
        elif self.question_target == 'CL11S':    return 'комментарий к речи премьер-министра'
        elif self.question_target == 'CL12S':    return 'комментарий к речи заместителя премьер-министра'
        elif self.question_target == 'CL21S':    return 'комментарий к речи члена правительства'
        elif self.question_target == 'CL22S':    return 'комментарий к речи секретаря правительства'
        elif self.question_target == 'CR11S':    return 'комментарий к речи лидера оппозиции'
        elif self.question_target == 'CR12S':    return 'комментарий к речи заместителя лидера оппозиции'
        elif self.question_target == 'CR21S':    return 'комментарий к речи члена оппозиции'
        elif self.question_target == 'CR22S':    return 'комментарий к речи секретаря оппозиции'
    
    # TODO: maybe factor out to a separate class
    @staticmethod
    def format_keyboard(question_target):
        '''
        A function that creates buttons with the interviewees and marks the buttons pointing at selected question target
        '''
        level = len(question_target)
        keyboard = []
        
        if level >= 1:
            buttons = ['задать вопрос', 'оставить комментарий']
            if question_target[0] == 'Q':
                buttons[0] = '[V] ' + buttons[0]
            else:
                buttons[1] = '[V] ' + buttons[1]
            keyboard.append(buttons)
        
        if level >= 2:
            buttons = ['всем', 'команде правительства', 'команде оппозиции']
            if question_target[1] == 'S':
                buttons[0] = '[V] ' + buttons[0]
            elif question_target[1] == 'L':
                buttons[1] = '[V] ' + buttons[1]
            else:
                buttons[2] = '[V] ' + buttons[2]
            keyboard.append(buttons)
        
        if level >= 3:
            buttons = ['команде', 'первому столу', 'второму столу']
            if question_target[2] == 'S':
                buttons[0] = '[V] ' + buttons[0]
            elif question_target[2] == '1':
                buttons[1] = '[V] ' + buttons[1]
            else:
                buttons[2] = '[V] ' + buttons[2]
            keyboard.append(buttons)
        
        if level >= 4:
            buttons = ['столу']
            if question_target[1:3] == 'L1':
                buttons.extend(('премьер-министру', 'заместителю ПМ'))
            elif question_target[1:3] == 'L2':
                buttons.extend(('члену правительства', 'секретарю'))
            elif question_target[1:3] == 'R1':
                buttons.extend(('лидеру оппозиции', 'заместителю ЛО'))
            elif question_target[1:3] == 'R2':
                buttons.extend(('члену оппозиции', 'секретарю'))
            
            if question_target[3] == 'S':
                buttons[0] = '[V] ' + buttons[0]
            elif question_target[3] == '1':
                buttons[1] = '[V] ' + buttons[1]
            else:
                buttons[2] = '[V] ' + buttons[2]
            keyboard.append(buttons)
        
        return keyboard
    
    @staticmethod
    @functools.lru_cache(maxsize=1)
    def question_target_buttons():
        'List of all buttons that change question target'
        return (set(flatten(AskingState.format_keyboard('QL1S')))
                    | set(flatten(AskingState.format_keyboard('QL2S')))
                    | set(flatten(AskingState.format_keyboard('QR1S')))
                    | set(flatten(AskingState.format_keyboard('QR2S'))))
    
    def is_question_target_button(self, message):
        return message in self.question_target_buttons()
    
    # TODO: maybe factor out to a separate class
    def change_question_target(self, message):
        'A function that changes question target and creates path to them'
        
        # Question target formatting
        # 1. Q/C - ask Question or leave a Comment
        # 2. L/R - supporting or another side
        # 3. 1/2 - desk number
        # 4. 1/2 - participant number
        #    S   - end at current step
        
        if message == 'задать вопрос':
            self.question_target = 'Q' + self.question_target[1:]
        elif message == 'оставить комментарий':
            self.question_target = 'C' + self.question_target[1:]
        elif message == 'всем':
            self.question_target = self.question_target[0] + 'S'
        elif message == 'команде правительства':
            self.question_target = self.question_target[:1] + 'L' + (self.question_target[2:] or 'S')
        elif message == 'команде оппозиции':
            self.question_target = self.question_target[:1] + 'R' + (self.question_target[2:] or 'S')
        elif message == 'первому столу':
            self.question_target = self.question_target[:2] + '1' + (self.question_target[3:] or 'S')
        elif message == 'второму столу':
            self.question_target = self.question_target[:2] + '2' + (self.question_target[3:] or 'S')
        elif message in ('премьер-министру', 'члену правительства', 'лидеру оппозиции', 'члену оппозиции'):
            self.question_target = self.question_target[:3] + '1S'
        elif message in ('заместителю ПМ', 'секретарю', 'заместителю ЛО'):
            self.question_target = self.question_target[:3] + '2S'
        else:
            logging.error(f'Do not know how to change question target according to {message}')
        
        logging.info(f'Changed question target to {self.question_target} in response to {message}')
    
    def run(self, message_info, reply, send_callback):
        chat_id, message, message_id = message_info
        
        if message == '/start':
            reply('Данные очищены. Для работы, пожалуйста, выполните команду /start ещё раз.')
            return StartState()
        elif message == '/touch':         # just asking bot to repeat its prompt
            pass
        elif message == '/stop':
            return SentinelState(self)    # saving current state so that it can be reused
        elif message == '/restart':
            os.startfile(__file__.replace('states.py', 'main.py'))
            return SentinelState(self)    # saving current state so that it can be reused
        elif message.startswith('I KNOW WHAT I DO '):
            self.question_target = message.split()[-1]
        elif message.startswith('[V] '):
            pass
        elif self.is_question_target_button(message):
            self.change_question_target(message)
        else:
            if len(message) < 4:
                reply('Длина вопроса должна быть не менее 4 символов.')
            else:
                # Sending ready question to teacher.
                
                question = html.escape(message)
                send_callback(
                    1463706336,
                    f'''
Отправлен {self.format_target()}: <b>{question}</b>
Автор: <b>{chat_id}</b>
                    '''.strip()
                )
                reply(f'Вопрос "<b>{question}</b>" переслан.')
        
        self.enter_state(message_info, reply, send_callback)
        return self
    
    def __repr__(self):
        return json.dumps([self.question_target, self.user_class])
    
    @staticmethod
    def load(state_repr):
        self = AskingState()
        self.question_target, self.user_class = json.loads(state_repr)
        return self


@RegisterState
class SentinelState(IState):
    '''
    State where bot needs to shutdown.
    '''
    def __init__(self, previous_state=None):
        logging.debug(f'State just before bot shutdown: {previous_state}')
        self.previous_state = previous_state
    
    def needs_message(self):
        return False
    
    def enter_state(self, message_info, reply, send_callback):
        reply('Stopping.')
    
    def run(self, message_info, reply, send_callback):
        return self
    
    def __repr__(self):
        if not self.previous_state:
            return ''
        
        return self.previous_state.__class__.__name__ + ':' + repr(self.previous_state)
    
    @staticmethod
    def load(state_repr):
        logging.info(f'Loading SentinelState: {state_repr}')
        
        if state_repr != 'None':
            return IState.load(state_repr)    # state just before SentinelState
        
        return SentinelState()


def format_donation_msg():
    if time.time() <= time.mktime(time.strptime('01.09.2023', '%d.%m.%Y')):
        return 'Вы можете купить в столовой плюшку Тунёву Андрею из 10А класса.'
    elif time.time() <= time.mktime(time.strptime('01.09.2024', '%d.%m.%Y')):
        return 'Вы можете купить в столовой плюшку Тунёву Андрею из 11 класса.'
    else:
        return 'К сожалению, способы доната не обновлялись слишком давно.'


def donation_middleware(backend, message_info):
    chat_id, incoming_text, incoming_id = message_info
    
    if incoming_text == '/donate':
        backend.send_message(chat_id, format_donation_msg(), reply=incoming_id)
        return True
    else:
        return False
